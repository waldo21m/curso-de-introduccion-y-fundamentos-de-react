Animar un grupo de elementos

Vamos ahora a cubrir el funcionamiento de Transition Group que es un componente
que se encarga de manejar los componentes de una lista y aplicarle animaciones
dependiendo de si se agregan o se eliminan de la lista misma.

¿Eso que tiene que ver con nuestro ejemplo? Vamos a recordar que en Home.js
tenemos un arreglo de lugares. Este es la lista o la colección y cada uno de
estos elementos que corresponde a un componente. Esto es tal cual como lo vemos
en la documentación, es casi la misma situación solo que con unos ligeros
cambios donde, en nuestro caso, es un poco más complejo.

La peculariedad o lo que es necesario es que el componente que estamos colocando
dentro de la lista o de la colección sea un CSSTransition para que 
TransitionGroup pueda hacer sus funciones y colocar como falso el prop in 
cuando eliminemos este componente de la lista y colocar como verdadero el prop
in cuando este se agregue.

Para ello entonces colocamos in={this.props.in} para dejar que sea el componente
padre el que defina valor del componente hijo (le inserte el valor).

Luego, colocamos el componente TransitionGroup donde queremos que ocurra la
animación (en este caso, reemplazamos el antiguo div por TransitionGroup).

TransitionGroup solo se encarga de asignar el valor correspondiente de 
this.props.in en el hijo. Cuando eliminamos el arreglo, this.props.in será
falso y cuando agregamos un arreglo, this.props.in será verdadero. Eso lo
podemos comprobar cuando retrasamos ligeramente la asignación del arreglo 
places.