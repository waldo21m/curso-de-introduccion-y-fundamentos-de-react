Crear app con create-react-app

Con la lección anterior tendríamos nuestro primer proyecto con React pero este
no luce con la estructura que nosotros definimos para un proyecto más robusto
y grande. Para eso, vamos a apoyarnos de una estructura que ya está
predefinida que de hecho está creada en FB de los mismos creadores de React
y la estructura se puede generar a través de una librería que se llama
create-react-app que podemos instalar a través de npm. Para instalar dicha
librería de forma global ejecutamos en la consola de comandos:
    $ npm install -g create-react-app

Nota: Si estamos dentro de la terminal y arrastramos la carpeta a dicha consola,
de forma automática apuntará a la carpeta.

Una vez instalado la librería, accedemos a nuestra carpeta y ejecutamos el
comando:
    $ create-react-app places 

Al finalizar la instalación del proyecto, se nos ofrece una lista de comandos
que podemos utilizar, en este caso es la siguiente:

    npm start
        Starts the development server.
    
    npm build
        Bundles the app into static files for production.
    
    npm test
        Starts the test runner.
    
    npm eject
        Removes this tool and copies build dependencies, configuration files
        and scripts into the app directory. If you do this, you can't go back!

Entramos al proyecto y ejecutamos:
    $ npm start

Con esto podemos ver la demo del proyecto.